#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from distutils.core import setup

requirements = ["parse >= 1.6.6", "numpy", "pyserial", "matplotlib"]

if sys.version_info < (3, 3):
    requirements.append("mock == 2.0.0")

setup(
    name='ctl200',
    version='0.9.0',
    packages=['ctl200', 'ctl200.test'],
    # package_dir={'': 'koheron'},
    scripts=['bin/ctl200_iv.py', 'bin/ctl200_status.py', 'bin/ctl200_tec.py'],
    url='https://gitlab.com/ep-oct/ctl200',
    license='GPL v3',
    author='Evangelos Rigas',
    author_email='e.rigas@cranfield.ac.uk',
    description="Python library for communicating with Koheron's CTL-200 laser",
    long_description=open('README.md').read(),
    platforms=["x86_64"],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GPL',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Engineering',
        'Topic :: Office/Business',
    ],
    install_requires=requirements,
)
