#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is the commands module

"""

from parse import search

# Parse format specifiers
# Type 	Characters Matched 	Output
# w 	Letters and underscore 	str
# W 	Non-letter and underscore 	str
# s 	Whitespace 	str
# S 	Non-whitespace 	str
# d 	Digits (effectively integer numbers) 	int
# D 	Non-digit 	str
# n 	Numbers with thousands separators (, or .) 	int
# % 	Percentage (converted to value/100.0) 	float
# f 	Fixed-point numbers 	float
# e 	Floating-point numbers with exponent e.g. 1.1e-10, NAN (all case insensitive) 	float
# g 	General number format (either d, f or e) 	float
# b 	Binary numbers 	int
# o 	Octal numbers 	int
# x 	Hexadecimal numbers (lower and upper case) 	int
# ti 	ISO 8601 format date/time e.g. 1972-01-20T10:21:36Z (“T” and “Z” optional) 	datetime
# te 	RFC2822 e-mail format date/time e.g. Mon, 20 Jan 1972 10:21:36 +1000 	datetime
# tg 	Global (day/month) format date/time e.g. 20/1/1972 10:21:36 AM +1:00 	datetime
# ta 	US (month/day) format date/time e.g. 1/20/1972 10:21:36 PM +10:30 	datetime
# tc 	ctime() format date/time e.g. Sun Sep 16 01:03:52 1973 	datetime
# th 	HTTP log format date/time e.g. 21/Nov/2011:00:07:11 +0000 	datetime
# ts 	Linux system log format date/time e.g. Nov 9 03:37:44 	datetime
# tt 	Time e.g. 10:21:36 PM -5:30 	time

__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

statuses = {
    '=>': 'success',
    '?>': 'command not understood',
    '!>': 'command failed to execute',
    '~>': 'in progress'
}


def parse_response(expression, response):
    if response is None:
        return -1
    try:
        res = search(expression, response)
        return res if res is not None else -1
    except ValueError:
        return -1


class Command:
    """The skeleton for all the commands

    Attributes
    ----------
    cmd: str
        Is the command that is going to be executed by the laser.

    cmdtype: {'instruction', 'query'}
        The type of command.

    args: bool
        If the commands requires arguments to be passed.

    response: str
        Is the search term to be used in order to parse the actual response.

    """
    _cmd = ''
    _cmdtype = ''
    _response = ''
    _args = False

    def __init__(self):
        """Init the Command class. Does nothing.

        """
        pass

    @property
    def cmd(self):
        """

        Returns
        -------
        str
            the string representation of the command

        """
        return self._cmd

    @property
    def cmdtype(self):
        """

        Returns
        -------
        {'instruction', 'query'}
            the type of the command

        """
        return self._cmdtype

    @property
    def args(self):
        """

        Returns
        -------
        bool
            Return ``True`` if the command to be sent
            needs extra arguments.

        """
        return self._args

    @property
    def response(self):
        """

        Returns
        -------
        str
            Is the search term that is going to be used
            to parse the response.

        """
        return self._response

    def parse(self, response):
        """"Parses the response to a value

        Returns
        -------
        any

        """
        return response


class Execute:
    """This class is used to send command to the CTL-200 through the CommLayer

    Attributes
    ----------
    comm: ctl200.comm.CommLayer
        the communication layer object

    """

    def __init__(self, comm):
        """ It initiates the class

        """
        self.comm = comm

    def __call__(self, cmd, *args):
        c = cmd()
        if c.args:
            assert c.check_arguments(*args), True
        if c.cmdtype == 'instruction':
            return c.parse(self.comm.send(c.cmd, *args))
        elif c.cmdtype == 'query':
            return c.parse(self.comm.query(c.cmd, *args))


class LaserOn(Command):
    """Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``lason`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``lason`` needs extra
        arguments in order to be executed.

    response: str
        The search term to parse the response.

    """

    _cmd = 'lason'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^d}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``lason`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: int
            When successful returns  0 or 1, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(state):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        state: int
            Should be set to 1 to enable the laser current.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert (state == 0) or (state == 1)
        return True


class TecOn(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``tecon`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``tecon`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'tecon'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^d}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``tecon`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: int
            When successful returns  0 or 1, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(state):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        state: int
            Should be set to 1 to enable the TEC current.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert (state == 0) or (state == 1)
        return True


class TempProtection(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``tprot`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``tprot`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'tprot'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^d}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``tprot`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: int
            When successful returns  0 or 1, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(state):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        state: int
            Should be set to 1 to enable the temperature protection.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert (state == 0) or (state == 1)
        return True


class InterlockOn(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``lckon`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``lckon`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'lckon'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^d}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``lckon`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: int
            When successful returns  0 or 1, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(state):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        state: int
            Should be set to 1 to enable the interlock functionality.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert (state == 0) or (state == 1)
        return True


class LaserCurrent(Command):
    """ Set the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``ilaser`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``ilaser`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'ilaser'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``ilaser`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the laser current in mA, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(current):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        current: float
            Laser current in mA.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(current, str), 'Current must be a number!'
        assert 0 <= current <= 125, 'Current must be a number between 0-125 mA!!!'
        return True


class ThermistorSetpoint(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``rtset`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``rtset`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'rtset'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``rtset`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the thermistor resistance setpoint in Ohms, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(resistance):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        resistance: float
            Thermistor resistance setpoint in Ohms (default value: 10000.0).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(resistance, str), 'Resistance must be a number!'
        assert 5000.0 <= resistance <= 15000.0, 'Resistance must be in range 5000-15000'
        return True


class TempSetpoint(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``tset`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``tset`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'tset'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``tset`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the temperature setpoint in degrees Celcius, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(temp):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        temp: float
            Temperature setpoint in degrees Celcius.

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(temp, str), 'Temperature must be a number!'
        assert 18.0 <= temp <= 35.0, 'Temperature must be in range 20-35!!!'
        return True


class ThermistorMin(Command):
    """ Enables the laser current.

    Attributes
    ----------
    self.cmd: str
        Is equal to ``rtmin`` and is the command
        that is going to be executed by the laser.

    self.cmdtype: str
        The type of command. Is equal to `instruction`

    self.args: bool
        Equals ``True`` as ``rtmin`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'rtmin'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``rtmin`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the minimum thermistor resistance in Ohms, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(resistance):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        resistance: float
            Minimum thermistor resistance in Ohms. If the temperature protection is enabled,
            the laser current is automatically disabled below this value (default value: 5000.0).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(resistance, str), 'Resistance must be a number!'
        assert 5000.0 <= resistance <= 14999.0, 'Resistance must be in range 5000-14999!'
        return True


class ThermistorMax(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``rtmax`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``rtmax`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'rtmax'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``rtmax`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the maximum thermistor resistance in Ohms, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(resistance):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        resistance: float
            Maximum thermistor resistance in Ohms. If the temperature protection is enabled,
            the laser current is automatically disabled above this value (default value: 15000.0).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(resistance, str), 'Resistance must be a number!'
        assert 5000.0 <= resistance <= 14999.0, 'Resistance must be in range 5000-14999!'
        return True


class TECVoltageMin(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``vtmin`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``vtmin`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'vtmin'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``vtmin`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the minimum TEC voltage in Volts, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(voltage):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        voltage: float
             Minimum TEC voltage in V (default value: -2.0).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(voltage, str), 'Voltage must be a number!'
        assert -2.0 <= voltage <= 0, 'Voltage must be in range -2-0!'
        return True


class TECVoltageMax(Command):
    """ Enables the laser current.

    Attributes
    ----------
    cmd: str
        Is equal to ``vtmax`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``vtmax`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'vtmin'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``vtmax`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the maximum TEC voltage in Volts, else returns -1

        """
        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(voltage):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        voltage: float
             Maximum TEC voltage in V (default value: 2.0).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert not isinstance(voltage, str), 'Voltage must be a number!'
        assert 0 <= voltage <= 2.0, 'Voltage must be in range 0-2!'
        return True


class PGain(Command):
    """ Set the proportional gain of the temperature controller

    Attributes
    ----------
    cmd: str
        Is equal to ``pgain`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``pgain`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'pgain'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``pgain`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the proportional gain of the temperature controller, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(pgain):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        pgain: float
             Proportional gain of the temperature controller (default value: 0.001).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert 0.0 <= pgain <= 1000
        return True


class IGain(Command):
    """ Set the integral gain of the temperature controller

    Attributes
    ----------
    cmd: str
        Is equal to ``igain`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``igain`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'igain'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``igain`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the integral gain of the temperature controller, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(igain):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        igain: float
             Integral gain of the temperature controller (default value: 0.0001).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert 0.0 <= igain <= 1000
        return True


class DGain(Command):
    """ Set the differential gain of the temperature controller

    Attributes
    ----------
    cmd: str
        Is equal to ``dgain`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `instruction`

    args: bool
        Equals ``True`` as ``dgain`` needs extra
        arguments in order to be executed.

    """

    _cmd = 'dgain'
    _cmdtype = 'instruction'
    _args = True
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``pgain`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the differential gain of the temperature controller, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1

    @staticmethod
    def check_arguments(dgain):
        """Asserts that the arguments that are going
        to be passed to the laser are correct.

        Parameters
        ----------
        dgain: float
              Differential gain of the temperature controller (default value: 0.005).

        Returns
        -------
        bool
            Returns ``True`` when the arguments are correct.

        Raises
        ------
        AssertionError

        """
        assert 0.0 <= dgain <= 1000
        return True


class LaserStatus(LaserOn):
    """This command queries the current increasing sweep
    configuration with an emphasis on optical
    frequency step.

    Attributes
    ----------
    cmd: str
        Is equal to ``lason`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `query`.

    """
    _cmd = 'lason'
    _cmdtype = 'query'
    _args = False


class Version(Command):
    """This command queries the version of the firmware.

    Attributes
    ----------
    cmd: str
        Is equal to ``version`` and is the command
        that is going to be executed by the laser.

    cmdtype: str
        The type of command. Is equal to `query`.

    """
    _cmd = 'version'
    _cmdtype = 'query'
    _args = False
    _response = '\r\nV{}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for ``version`` command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: str or int
            When successful returns the version of CTL-200, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1


class StatusQuery(Command):
    """Generic class for quering a laser attribute.

    Attributes
    ----------
    cmdtype: str
        The type of command. Is equal to `query`

    """

    _cmdtype = 'query'
    _response = '{:^f}\r\n>>'

    def parse(self, response):
        """Parses CTL-200 response for a status command

        Parameters
        ----------
        response: str
            This is the response of the CTL-200 on the command

        Returns
        -------
        res: float
            When successful returns the queried attribute value, else returns -1

        """

        res = parse_response(self.response, response)
        return res.fixed[0] if res is not -1 else -1


class LaserVoltage(StatusQuery):
    """This command queries the laser voltage.

    Attributes
    ----------
    cmd: str
        Is equal to ``vlaser`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'vlaser'


class PhotodiodeCurrent(StatusQuery):
    """This command queries the photodiode current.

    Attributes
    ----------
    cmd: str
        Is equal to ``iphd`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'iphd'


class ThermistorResistance(StatusQuery):
    """This command queries the actual value of the thermistor resistance in Ohms.

    Attributes
    ----------
    cmd: str
        Is equal to ``rtact`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'rtact'


class TecCurrent(StatusQuery):
    """This command queries the TEC current in Amps.

    Attributes
    ----------
    cmd: str
        Is equal to ``itec`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'itec'


class TecVoltage(StatusQuery):
    """This command queries the TEC voltage in Volts.

    Attributes
    ----------
    cmd: str
        Is equal to ``vtec`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'vtec'


class Temp(StatusQuery):
    """This command queries the TEC voltage in Volts.

    Attributes
    ----------
    cmd: str
        Is equal to ``tset`` and is the command that is going to be executed by the laser.

    """
    _cmd = 'tset'
