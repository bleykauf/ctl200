#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Init of Koheron CTL-200 LASER Library

"""

__all__ = ['laser.Laser']
__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "3.0.0-58564d5"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"
