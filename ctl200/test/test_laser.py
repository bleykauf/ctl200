#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test Suite for the laser module ``ctl200.laser``.

"""

import sys
from unittest import TestCase

import ctl200.laser

if sys.version_info > (3, 3):
    import unittest.mock as mock
else:
    import mock

__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

responses = {'version': 'version\r\nV0.7\r\n>>',
             'lason': 'lason\r\n0\r\n>>',
             'pgain': 'pgain\r\n0.1\r\n>>'}

test_cmd = ''


class TestLaser(TestCase):
    las = ctl200.laser.Laser('/dev/ttyUSB0')

    @staticmethod
    def mock_readto(*_):
        global responses, test_cmd
        return responses.get(test_cmd)

    @staticmethod
    def mock_write(*_):
        pass

    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def test_version(self):
        global test_cmd
        test_cmd = 'version'
        r = self.las.version
        self.assertEqual(r, '0.7')

    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def test_laser_status(self):
        global test_cmd
        test_cmd = 'lason'
        r = self.las.laser_status
        self.assertEqual(r, 0)

    @staticmethod
    def mock_send_gain(*_):
        return 'pgain\r\n0.1\r\n>>'

    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def test_pgain(self):
        global test_cmd
        test_cmd = 'pgain'
        self.assertEqual(self.las.pgain, 0.001)
        self.las.pgain = 0.1
        self.assertEqual(self.las.pgain, 0.1)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_gain)
    def test_igain(self):
        self.assertEqual(self.las.igain, 0.0001)
        self.las.igain = 0.1
        self.assertEqual(self.las.igain, 0.1)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_gain)
    def test_dgain(self):
        self.assertEqual(self.las.dgain, 0.005)
        self.las.dgain = 0.1
        self.assertEqual(self.las.dgain, 0.1)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_gain)
    def test_setreset_pid(self):
        self.las.set_pid(0.1, 0.1, 0.5)
        self.assertEqual(self.las.pid, (0.1, 0.1, 0.5))
        self.las.reset_pid()
        self.assertEqual(self.las.pid, (0.001, 0.0001, 0.005))

    @staticmethod
    def mock_send_current(*_):
        return 'ilaser\r\n10.0\r\n>>'

    def set_current(self, current):
        self.las.laser_current = current

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_current)
    def test_laser_current(self):
        self.assertEqual(self.las.laser_current, 0)
        self.las.laser_current = 12
        self.assertEqual(self.las.laser_current, 12)
        self.assertRaises(AssertionError, self.set_current, 150)

    @staticmethod
    def mock_send_temp(*_):
        return 'tset\r\n25.5\r\n>>'

    def set_temp(self, temp):
        self.las.temp_setpoint = temp

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_temp)
    def test_temp_setpoint(self):
        self.assertEqual(self.las.temp_setpoint, 0)
        self.las.temp_setpoint = 30
        self.assertEqual(self.las.temp_setpoint, 30)
        self.assertRaises(AssertionError, self.set_temp, 150)

    @staticmethod
    def mock_open(*args):
        pass

    @staticmethod
    def mock_open_exception(*args):
        raise Exception

    @mock.patch('ctl200.comm.CommLayer.open', mock_open)
    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def open_laser_exception(self):
        with ctl200.laser.Laser('/dev/ttyUSB0') as las:
            raise Exception('Whooo, I am an exception!')

    @mock.patch('ctl200.comm.CommLayer.open', mock_open)
    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def open_laser_keyboard(self):
        with ctl200.laser.Laser('/dev/ttyUSB0') as las:
            raise KeyboardInterrupt('Sorry for interrupting!')

    def test_exit_with_exception(self):
        self.assertRaises(Exception, self.open_laser_exception)

    def test_exit_with_keyboard_interrupt(self):
        self.open_laser_keyboard()

    @mock.patch('ctl200.comm.CommLayer.open', mock_open_exception)
    @mock.patch('ctl200.comm.CommLayer.readto', mock_readto)
    @mock.patch('ctl200.comm.CommLayer.write', mock_write)
    def open_laser(self):
        with ctl200.laser.Laser('/dev/ttyUSB0') as las:
            pass

    def test_laser_open_exception(self):
        self.assertRaises(Exception, self.open_laser)
