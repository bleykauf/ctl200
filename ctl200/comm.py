#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
module comm

"""
from __future__ import print_function, division

import serial

__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"


class CommLayer(serial.Serial):
    """CommLayer class wraps around :obj:`serial.SerialBase` class providing extra functions
    to facilitate with the Koheron CTL-200 communication.

    """
    end = b'>>'
    ret = "\r\n"

    def readto(self):
        """Read from serial until you see '>>'.
        It wraps :obj:`self.read_until`.

        """
        return self.read_until(self.end).decode()

    def writer(self, buffer):
        """

        Parameters
        ----------
        buffer

        """
        self.write(buffer.encode())

    def send(self, cmd, *args):
        """

        Parameters
        ----------
        cmd

        args

        Returns
        -------

        """
        if len(args) > 0:
            command = '{} {} {}'.format(cmd, ' '.join(str(i) for i in args), self.ret)
            self.writer(command)
        else:
            self.writer(cmd + self.ret)
        return self.readto()

    def query(self, cmd, *args):
        """

        Parameters
        ----------
        cmd: str
            the command to send

        args: list
            extra arguments to pass along the command

        Returns
        -------
        r: bytes
            the response of the query

        """
        if len(args) > 0:
            r = self.send(cmd, *args)
        else:
            r = self.send(cmd)
        return r

    def connect(self, port, baudrate=115200, timeout=1):
        """

        Parameters
        ----------
        port: str
            This is the port of the CTL-200. It can be any valid port address in the form of `/dev/ttyX`.
        baudrate: int
            The baudrate for the serial communication, defaults to 115200.
        timeout: int
            The time in seconds to abort the operation if unsuccesful, defaults to 1.

        """
        print('\rConnecting to', port, end='')
        try:
            self.port = port
            self.baudrate = baudrate
            self.timeout = timeout
            self.open()
            print('\rConnected.')
        except Exception:
            raise Exception
        self.query('')
