#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
module ctl200

"""
from __future__ import print_function, division
import traceback
from sys import stdout
from ctl200.comm import CommLayer
from ctl200.commands import *

__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

__all__ = ['Laser']


class Laser(object):
    """This is the main class for the ctl200 ctl200.
    It implements macro functions to set the ctl200 parameters.

    """

    _pgain = 0.001
    _igain = 0.0001
    _dgain = 0.005
    _laser_current = 0
    _tset = 0

    def __init__(self, port):
        self.comm = CommLayer()
        self.port = port
        self.execute = Execute(self.comm)

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, type, value, etraceback):
        print('\n\rExiting...', end='')
        if value is not None:
            if not isinstance(value, KeyboardInterrupt):
                print('\rSetting laser status to OFF...', end='')
                self.laser_status = 0
                raise type(value).with_traceback(etraceback)
        self.close()
        print('\rConnection closed.')
        return True

    def connect(self):
        """
        Connects to the laser

        """
        self.comm.connect(self.port)

    def close(self):
        """Closes the connection with the laser

        """
        self.comm.close()

    @property
    def version(self):
        """Returns the version of CTL-200 firmware

        See Also
        --------
        ctl200.commands.Version

        Returns
        -------
        str or int
            When successful it returns the version, else returns -1

        """
        return self.execute(Version)

    @property
    def laser_status(self):
        """Return the status of the laser.

        Returns
        -------
        int
            When successful it returns either 0 (off) or 1 (on), else returns -1

        See Also
        --------
        ctl200.commands.LaserStatus

        """

        return self.execute(LaserStatus)

    @laser_status.setter
    def laser_status(self, state):
        """Sets the laser state.

        Parameters
        ----------
        state: int
            The state of the laser, 0 for off, 1 for on (integer,0-1).

        See Also
        --------
        ctl200.commands.LaserOn

        """
        self.execute(LaserOn, state)

    @property
    def laser_current(self):
        """Return the laser current in milli-ampere.

        Returns
        -------
        float
            Returns the laser current in milli-ampere

        See Also
        --------
        ctl200.commands.LaserCurrent

        """

        return self._laser_current

    @laser_current.setter
    def laser_current(self, current):
        """Sets the laser current.

        Parameters
        ----------
        current: float
            The laser current in milli-ampere.


        See Also
        --------
        ctl200.commands.LaserCurrent

        """
        assert LaserCurrent.check_arguments(current)
        self._laser_current = current
        self.execute(LaserCurrent, current)

    @property
    def temp_setpoint(self):
        """Return the laser temperature in Celcius.

        Returns
        -------
        float
            Returns the laser temperature in Celcius.


        """
        return self._tset

    @temp_setpoint.setter
    def temp_setpoint(self, temp):
        """Sets the laser temperature in Celcius.

        Parameters
        ----------
        current: float
            The laser temperature in Celcius.

        See Also
        --------
        ctl200.commands.TempSetpoint

        """
        assert TempSetpoint.check_arguments(temp)
        self._tset = temp
        self.execute(TempSetpoint, temp)

    @property
    def laser_temp(self):
        """Return the actual laser temperature in Celcius.

        Returns
        -------
        float
            Returns the actual laser temperature in Celcius.

        See Also
        --------
        ctl200.commands.Temp

        """

        return self.execute(Temp)

    @property
    def pgain(self):
        """Return the proportional gain of the TEC driver.

        Returns
        -------
        float
            Returns the proportional gain of the TEC driver.

        """

        return self._pgain

    @pgain.setter
    def pgain(self, gain):
        """Sets the proportional gain of the TEC driver.

        Parameters
        ----------
        gain: float
            The proportional gain of the TEC driver.


        See Also
        --------
        ctl200.commands.PGain

        """
        assert PGain.check_arguments(gain)
        self._pgain = gain
        self.execute(PGain, gain)

    @property
    def igain(self):
        """Return the integral gain of the TEC driver.

        Returns
        -------
        float
            Returns the integral gain of the TEC driver.

        """

        return self._igain

    @igain.setter
    def igain(self, gain):
        """Sets the integral gain of the TEC driver.

        Parameters
        ----------
        gain: float
            The integral gain of the TEC driver.

        See Also
        --------
        ctl200.commands.IGain

        """
        assert IGain.check_arguments(gain)
        self._igain = gain
        self.execute(IGain, gain)

    @property
    def dgain(self):
        """Return the differential gain of the TEC driver.

        Returns
        -------
        float
            Returns the differential gain of the TEC driver.

        """

        return self._dgain

    @dgain.setter
    def dgain(self, gain):
        """Sets the differential gain of the TEC driver.

        Parameters
        ----------
        gain: float
            The differential gain of the TEC driver.

        See Also
        --------
        ctl200.commands.IGain

        """
        assert DGain.check_arguments(gain)
        self._dgain = gain
        self.execute(DGain, gain)

    def set_pid(self, p, i, d):
        self.pgain = p
        self.igain = i
        self.dgain = d

    def reset_pid(self):
        self.pgain = 0.001
        self.igain = 0.0001
        self.dgain = 0.005

    @property
    def pid(self):
        return self.pgain, self.igain, self.dgain

    def set_tec(self, state):
        """Sets the TEC state.

        Parameters
        ----------
        state: int
            The state of the TEC, 0 for off, 1 for on (integer,0-1).

        See Also
        --------
        ctl200.commands.TecOn

        """
        self.execute(TecOn, state)

    @property
    def rt_resistance(self):
        """Return the actual resistance of the thermistor.

        Returns
        -------
        str or int
            When successful it returns the thermistor resistance in Ohms, else returns -1

        See Also
        --------
        ctl200.commands.ThermistorResistance

        """

        return self.execute(ThermistorResistance)

    @rt_resistance.setter
    def rt_resistance(self, resistance):
        """Sets the thermistor setpoint resistance.

        Parameters
        ----------
        resistance: float
            The state of the laser, 0 for off, 1 for on (integer,0-1).

        See Also
        --------
        ctl200.commands.ThermistorSetpoint

        """
        self.execute(ThermistorSetpoint, resistance)

    @property
    def tec_voltage(self):
        """Return the TEC voltage.

        Returns
        -------
        str or int
            When successful it returns the TEC voltage in Volts, else returns -1

        See Also
        --------
        ctl200.commands.TecVoltage

        """

        return self.execute(TecVoltage)

    @property
    def tec_current(self):
        """Return the TEC current.

        Returns
        -------
        str or int
            When successful it returns the TEC current in Ampere, else returns -1

        See Also
        --------
        ctl200.commands.TecCurrent

        """

        return self.execute(TecCurrent)

    @property
    def laser_voltage(self):
        """Return the laser voltage.

        Returns
        -------
        str or int
            When successful it returns the laser voltage in Volts, else returns -1

        See Also
        --------
        ctl200.commands.LaserVoltage

        """

        return self.execute(LaserVoltage)

    @property
    def photo_current(self):
        """Return the photodiode current.

        Returns
        -------
        str or int
            When successful it returns the photodiode current in milli-ampere, else returns -1

        See Also
        --------
        ctl200.commands.PhotodiodeCurrent

        """

        return self.execute(PhotodiodeCurrent)

    def send_scpi(self, scpi):
        """Allows to send any SCPI command to the laser

        Parameters
        ----------
        scpi: str
            The command you want to execute with all the arguments

        Returns
        -------
        str
           This is the response as it came from the laser, unaltered.
        """
        return self.comm.query(scpi)
