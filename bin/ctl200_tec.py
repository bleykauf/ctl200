#!/usr/bin/env python
# coding=utf-8
"""
Plot TEC characteristic

"""
import time
import argparse
import numpy as np
import matplotlib.pyplot as plt

from ctl200 import laser

__all__ = ['laser.Laser']
__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

cp_short = "CTL-200 temperature curves, v0.9.0\nCopyright (C) 2019 Evangelos Rigas.\n"
cp = """CTL-200 temperature curves, v0.9.0
Copyright (C) 2019 Evangelos Rigas.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."""

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description='CTL-200 laser driver temperature curves, v0.9.0',
                                 epilog=cp)
parser.add_argument("-n", "--points", type=int, default=100, help="Number of data points (default: 100).")
parser.add_argument("-p", "--port", type=str, default='/dev/ttyUSB0', help="Serial port (default: /dev/ttyUSB0).")
parser.add_argument("-c", "--copyright", action='store_true', help="Display copyright.")

if __name__ == "__main__":
    args = parser.parse_args()

    if args.copyright:
        print(cp)
        exit(0)

    tec_ivs = np.zeros((2, args.points, 3))
    tsets = np.linspace(19, 30, args.points)

    with laser.Laser(args.port) as las:
        las.dgain = 0
        las.laser_current = 0
        las.laser_status = 1

        for j, cur in enumerate([50, 100]):
            print('\nSetting laser current to {} mA'.format(cur))
            las.laser_current = cur
            las.temp_setpoint = 19
            print('\rStabilising temperature...', end='')
            time.sleep(5)

            for i, t in enumerate(tsets):
                las.temp_setpoint = t
                time.sleep(0.5)
                tec_ivs[j, i, :] = [las.tec_current, las.tec_voltage, las.laser_temp]
                print('\rI: {:2.2f} A, V: {:2.2f} V, T: {:2.2f} C'.format(*tec_ivs[j, i, :]), end='')
        las.laser_current = 0
        las.laser_status = 0

        fig, (ax1, ax2) = plt.subplots(1, 2)
        for j, cur in enumerate([50, 100]):
            ax1.plot(tec_ivs[j, :, 2], tec_ivs[j, :, 0], label='{} mA'.format(cur))
            ax2.plot(tec_ivs[j, :, 2], tec_ivs[j, :, 1], label='{} mA'.format(cur))

        ax1.set_title('CTL-200 TEC current vs Temperature')
        ax1.set_xlabel('Temperature in C')
        ax1.set_ylabel('TEC current in A')
        ax1.legend()

        ax2.set_title('CTL-200 TEC voltage vs Temperature')
        ax2.set_xlabel('Temperature in C')
        ax2.set_ylabel('TEC voltage in V')
        ax2.legend()
        plt.show()
    exit(0)
