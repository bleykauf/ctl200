#!/usr/bin/env python
# coding=utf-8
"""
Plot current vs voltage characteristic

"""
import time
import argparse
import numpy as np

from ctl200 import laser

__all__ = ['laser.Laser']
__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

cp_short = "CTL-200 I-V curves, v0.9.0\nCopyright (C) 2019 Evangelos Rigas.\n"
cp = """CTL-200 I-V curves, v0.9.0
Copyright (C) 2019 Evangelos Rigas.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."""

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description='CTL-200 laser driver I-V curves, v0.9.0',
                                 epilog=cp)
parser.add_argument("-n", "--points", type=int, default=100, help="Number of data points (default: 100).")
parser.add_argument("-p", "--port", type=str, default='/dev/ttyUSB0', help="Serial port (default: /dev/ttyUSB0).")
parser.add_argument("--tec", action='store_true', help="Store TEC voltage and current.")
parser.add_argument("-o", "--filename", type=str, default='iv_curves',
                    dest='fname', help="Output filename (default: 'iv_curves')")
parser.add_argument("-g", "--plot", action='store_true', help="Plot curves using matplotlib.")
parser.add_argument("-c", "--copyright", action='store_true', help="Display copyright.")

disp = "I: {:.2f}, V: {:.2f}, PD: {:.2f}, R: {:.2f}, T: {:.2f}"

if __name__ == "__main__":
    args = parser.parse_args()

    if args.copyright:
        print(cp)
        exit(0)

    tsets = np.array([20.0, 25.0, 30.0])
    ilasers = np.linspace(0, 120, args.points)
    vlasers = np.zeros((np.size(ilasers), np.size(tsets)))
    iphds = np.zeros_like(vlasers)

    if args.tec:
        tec_ivs = np.zeros((np.size(ilasers), np.size(tsets), 2))

    with laser.Laser(args.port) as las:
        las.dgain = 0
        las.laser_current = 0
        las.laser_status = 1

        for j, tset in enumerate(tsets):
            las.laser_current = ilasers[0]
            las.temp_setpoint = tset
            print('Wait to reach temperature {} ({})'.format(tset, las.laser_temp))
            t = 0
            while not np.isclose(tset, las.laser_temp):
                print('\rTemp: {}'.format(las.laser_temp), end='')
                time.sleep(1)
                t += 1
                if t > 30:
                    break
            print('')
            for i, ilaser in enumerate(ilasers):
                las.laser_current = ilaser
                time.sleep(0.1)
                vlaser = las.laser_voltage
                iphd = las.photo_current
                print(disp.format(ilaser, vlaser, iphd, las.rt_resistance, las.laser_temp))
                vlasers[i, j] = vlaser
                iphds[i, j] = iphd
                if args.tec:
                    tec_ivs[i, j, :] = [las.tec_current, las.tec_voltage]

        las.laser_current = 0
        las.laser_status = 0
        if args.tec:
            np.save("{}_tec.npy".format(args.fname), tec_ivs)
        np.save("{}.npy".format(args.fname), (tsets, ilasers, vlasers))

        if args.plot:
            import matplotlib.pyplot as plt

            fig, ax = plt.subplots()
            for i, j in enumerate(tsets):
                ax.plot(ilasers, vlasers[:, i], label=j)
            ax.legend()
            ax.set_title('CTL-200 laser I-V curves')
            ax.set_xlabel('Laser current in mA')
            ax.set_ylabel('Laser voltage in V')
            plt.show()

    exit(0)
