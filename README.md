Repo for CTL-200 laser controller library
------------------------------
![build](https://gitlab.com/erigas/ctl200/badges/master/pipeline.svg)
![build](https://gitlab.com/erigas/ctl200/badges/master/coverage.svg?job=coverage)

# Introduction

`ctl200` is a python library for communicating with [CTL-200](https://www.koheron.com/photonics/ctl200-digital-laser-controller) laser controller from Koheron.
For the communication it uses the serial port available on the micro usb.

![cont](https://www.koheron.com/static/images/ctl200/CTL200.jpg)

# Documentation

The library documentation is available at: https://erigas.gitlab.io/ctl200/

# Coverage
The code coverage of the package is available at: https://erigas.gitlab.io/ctl200/coverage/

# Installation

To install run the following in a terminal:
```bash
pip install git+https://gitlab.com/erigas/ctl200.git
```
# Usage

The snippet below shows an example of how to use the library to query and control the laser.
It assumes that the serial port is `/dev/ttyUSB0`.

```python
from time import sleep
from ctl200 import laser

with laser.Laser('/dev/ttyUSB0') as las:
    # Set the current to 100 mA at 25 degrees Celcius and enable the laser
    las.temp_setpoint = 25 
    las.laser_current = 100
    las.laser_status = 1 

    sleep(10) # wait 10 seconds

    # Query the values of different parameters
    print('Laser current:   {:8.2f}       mA'.format(las.laser_current))
    print('TEC voltage:     {: 10.4f}     V'.format(las.tec_voltage))
    print('TEC current:     {: 10.4f}     A'.format(las.tec_current))
    print('PD current:         {: 4.2e}   mA'.format(las.photo_current))
    print('Thermistor:    {: 12.4f}     Ohms'.format(las.rt_resistance))
    print('Temperature:     {: 8.2f}       C'.format(las.laser_temp))
    
    # Disable the laser and set the current to 0 mA
    las.laser_current = 0
    las.laser_status = 0


```